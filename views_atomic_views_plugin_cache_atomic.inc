<?php
// $Id$
/**
 * Thomas Gielfeldt <thomas@gielfeldt.com>
 * Copyright 2011
 *
 * @file
 *
 * Views cache plugin file
 *
 * Copyright 2011
 *
 */

/**
 * Caching plugin that provides static caching for sake of conistency.
 */
class views_atomic_views_plugin_cache_atomic extends views_plugin_cache {
  static private $atomic = array();
  
  function cache_start() { 
    /* do nothing */
  }

  function summary_title() {
    return t('Atomic');
  }

  function cache_get($type) {
    if ($type != 'results') return FALSE;
    $key = $this->get_results_key();
    if (isset(self::$atomic[$key])) {
      $this->view->result = array_slice(self::$atomic[$key]['result'], $this->view->pager['offset'], $this->view->pager['items_per_page']);
      $this->view->total_rows = count($this->view->result);
      $this->view->execute_time = 0;
      return TRUE;
    }
    else {
      self::$atomic[$key]['pager'] = $this->view->pager;
      $this->view->pager['items_per_page'] = 0;
      $this->view->pager['offset'] = 0;
      return FALSE;
    }
  }

  function cache_set($type) { 
    if ($type != 'results') return FALSE;
    $key = $this->get_results_key();
    if (!isset(self::$atomic[$key]['result'])) {
      self::$atomic[$key]['result'] = $this->view->result;
      $this->view->pager['offset'] = self::$atomic[$key]['pager']['offset'];
      $this->view->pager['items_per_page'] = self::$atomic[$key]['pager']['items_per_page'];
      $this->view->result = array_slice($this->view->result, $this->view->pager['offset'], $this->view->pager['items_per_page']);
      $this->view->total_rows = count($this->view->result);
    }
  }
}
